var express = require('express');
var router = express.Router();
var {WidgetSize} = require('../../models');

/* GET list of widget sizes */
router.get('/', function(req, res) {
    WidgetSize.findAll().then(function(widgetSizes){
        res.json(widgetSizes);
    });
});

/* GET a particular widget size */
router.get('/:id', function(req, res) {
    WidgetSize.findOne({ where: { id:req.params.id } }).then(function(widgetSize){
        res.json(widgetSize);
    });
});

/* POST a new widget size */
router.post('/', function(req, res) {
    var newSize = req.body;
    WidgetSize.create(newSize).then(function(widgetSize){
        res.json(widgetSize);
    });
});

/* PUT an update to a widget size */
router.put('/:id', function(req, res) {
    var updatedSize = req.body;
    WidgetSize.update(updatedSize, { where: { id: req.params.id } }).then(function(widgetSize){
        res.json(widgetSize);
    });
});


/* DELETE a widget size */
router.delete('/:id', function(req, res) {
    WidgetSize.destroy({ where: { id: req.params.id } }).then(function(widgetSize){
        res.status(200);
        res.end();
    });
});

module.exports = router;
