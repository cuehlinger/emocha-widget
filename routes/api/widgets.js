var express = require('express');
var router = express.Router();
var {Widget} = require('../../models');

/* GET list of widgets */
router.get('/', function(req, res) {
    Widget.findAll().then(function(widgets){
        res.json(widgets);
    });
});

/* GET a particular widget */
router.get('/:id', function(req, res) {
    Widget.findOne({ where: { id:req.params.id } }).then(function(widget){
        res.json(widget);
    });
});

/* POST a new widget */
router.post('/', function(req, res) {
    var newWidget = req.body;
    Widget.create(newWidget).then(function(widget){
        res.json(widget);
    });
});

/* PUT an update to a widget */
router.put('/:id', function(req, res) {
    var updatedWidget = req.body;
    Widget.update(updatedWidget, { where: { id: req.params.id } }).then(function(widget){
        res.json(widget);
    });
});


/* DELETE a widget */
router.delete('/:id', function(req, res) {
    Widget.destroy({ where: { id: req.params.id } }).then(function(widget){
        res.status(200);
        res.end();
    });
});

module.exports = router;
