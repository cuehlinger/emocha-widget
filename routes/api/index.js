var express = require('express');
var router = express.Router();

router.use('/types', require('./types'));
router.use('/finishes', require('./finishes'));
router.use('/sizes', require('./sizes'));
router.use('/widgets', require('./widgets'));

module.exports = router;
