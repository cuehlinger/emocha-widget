var express = require('express');
var router = express.Router();
var {WidgetType} = require('../../models');

/* GET list of widget types */
router.get('/', function(req, res) {
    WidgetType.findAll().then(function(widgetTypes){
        res.json(widgetTypes);
    });
});

/* GET a particular widget type */
router.get('/:id', function(req, res) {
    WidgetType.findOne({ where: { id:req.params.id } }).then(function(widgetType){
        res.json(widgetType);
    });
});

/* POST a new widget type */
router.post('/', function(req, res) {
    var newType = req.body;
    WidgetType.create(newType).then(function(widgetType){
        res.json(widgetType);
    });
});

/* PUT an update to a widget type */
router.put('/:id', function(req, res) {
    var updatedType = req.body;
    WidgetType.update(updatedType, { where: { id: req.params.id } }).then(function(widgetType){
        res.json(widgetType);
    });
});


/* DELETE a widget type */
router.delete('/:id', function(req, res) {
    WidgetType.destroy({ where: { id: req.params.id } }).then(function(widgetType){
        res.status(200);
        res.end();
    });
});

module.exports = router;
