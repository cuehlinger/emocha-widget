var express = require('express');
var router = express.Router();
var {WidgetFinish} = require('../../models');

/* GET list of widget finishes */
router.get('/', function(req, res) {
    WidgetFinish.findAll().then(function(widgetFinishes){
        res.json(widgetFinishes);
    });
});

/* GET a particular widget finish */
router.get('/:id', function(req, res) {
    WidgetFinish.findOne({ where: { id:req.params.id } }).then(function(widgetFinish){
        res.json(widgetFinish);
    });
});

/* POST a new widget finish */
router.post('/', function(req, res) {
    var newFinish = req.body;
    WidgetFinish.create(newFinish).then(function(widgetFinish){
        res.json(widgetFinish);
    });
});

/* PUT an update to a widget finish */
router.put('/:id', function(req, res) {
    var updatedFinish = req.body;
    WidgetFinish.update(updatedFinish, { where: { id: req.params.id } }).then(function(widgetFinish){
        res.json(widgetFinish);
    });
});


/* DELETE a widget finish */
router.delete('/:id', function(req, res) {
    WidgetFinish.destroy({ where: { id: req.params.id } }).then(function(widgetFinish){
        res.status(200);
        res.end();
    });
});

module.exports = router;
