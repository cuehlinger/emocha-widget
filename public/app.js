var app = angular.module('app', ['ui.router', 'ui.bootstrap']);

app.factory('widgetFactory', function($http, $q){
    return {
        getAllWidgets: function(){
            return $http.get('/api/widgets').then(function(response){
                var widgets = response.data;
                return $q.all(widgets.map(function(widget) {
                    var promises = [];

                    promises.push($http.get('/api/types/' + widget.type));
                    promises.push($http.get('/api/finishes/' + widget.finish));
                    promises.push($http.get('/api/sizes/' + widget.size));

                    return $q.all(promises).then(function(results){
                        widget.type = results[0].data;
                        widget.finish = results[1].data;
                        widget.size = results[2].data;
                        return widget;
                    });
                }));
            });
        },
        getAllTypes: function(){
            return $http.get('/api/types')
                        .then(function(response){
                            return response.data;
                        });
        },
        getAllFinishes: function(){
            return $http.get('/api/finishes')
                        .then(function(response){
                            return response.data;
                        });
        },
        getAllSizes: function(){
            return $http.get('/api/sizes')
                        .then(function(response){
                            return response.data;
                        });
        },
        postNewWidget: function(widget) {
            return $http.post('/api/widgets', widget)
                        .then(function(response){
                            return response.data;
                        });
        }
    };
});

app.controller('AppController', function($scope, widgetFactory){
    $scope.widgets = [];
    widgetFactory.getAllWidgets().then(function(widgets) {
        $scope.widgets = widgets;
    });

    $scope.types = [];
    widgetFactory.getAllTypes().then(function(types) {
        $scope.types = types;
    });

    $scope.finishes = [];
    widgetFactory.getAllFinishes().then(function(finishes) {
        $scope.finishes = finishes;
    });

    $scope.sizes = [];
    widgetFactory.getAllSizes().then(function(sizes) {
        $scope.sizes = sizes;
    });
    
    $scope.newWidget = {};
    $scope.makeNewWidget = function(newWidget) {
        console.log(newWidget);
        widgetFactory.postNewWidget(newWidget)
            .then(function(){
                widgetFactory.getAllWidgets()
                .then(function(widgets) {
                    $scope.widgets = widgets;
                });
            });
    }

});