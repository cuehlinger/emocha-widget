'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('WidgetSizes', [{
        name: 'Invisibly Small',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'Majestically Medium',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'Galactically Huge',
        createdAt: new Date(),
        updatedAt: new Date()
      },], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('WidgetSizes', null, {});
  }
};
