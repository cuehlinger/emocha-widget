'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('WidgetFinishes', [{
        name: 'Red',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'White',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'Chrome',
        createdAt: new Date(),
        updatedAt: new Date()
      },], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('WidgetFinishes', null, {});
  }
};
