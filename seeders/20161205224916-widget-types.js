'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('WidgetTypes', [{
        name: 'Prime',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'Elite',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        name: 'Extreme Edition',
        createdAt: new Date(),
        updatedAt: new Date()
      },], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('WidgetTypes', null, {});
  }
};
