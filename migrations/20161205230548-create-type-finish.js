'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('TypeFinishes', { 
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetTypes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      finish: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetFinishes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }

    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('TypeFinishes');
  }
};
