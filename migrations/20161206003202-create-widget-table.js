'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('Widgets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      type: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetTypes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      finish: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetFinishes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      size: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetSizes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      }
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('Widgets');
  }
};
