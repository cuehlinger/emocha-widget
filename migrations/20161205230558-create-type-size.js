'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('TypeSizes', { 
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetTypes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      size: {
        type: Sequelize.INTEGER,
        references: {
            model: 'WidgetSizes',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }

    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('TypeSizes');
  }
};
