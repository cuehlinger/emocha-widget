'use strict';
module.exports = function(sequelize, DataTypes) {
  var WidgetFinish = sequelize.define('WidgetFinish', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return WidgetFinish;
};