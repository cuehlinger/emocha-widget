'use strict';
module.exports = function(sequelize, DataTypes) {
  var WidgetType = sequelize.define('WidgetType', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return WidgetType;
};