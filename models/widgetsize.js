'use strict';
module.exports = function(sequelize, DataTypes) {
  var WidgetSize = sequelize.define('WidgetSize', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return WidgetSize;
};