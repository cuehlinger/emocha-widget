'use strict';

var WidgetType = require('./widgettype');
var WidgetFinish = require('./widgetfinish');
var WidgetSize = require('./widgetsize');

module.exports = function(sequelize, DataTypes) {
  var Widget = sequelize.define('Widget', {
    type: {
        type: DataTypes.INTEGER,
        references: {
            model: WidgetType,
            key: 'id'
        }
    },
    finish: {
        type: DataTypes.INTEGER,
        references: {
            model: WidgetFinish,
            key: 'id'
        }
    },
    size: {
        type: DataTypes.INTEGER,
        references: {
            model: WidgetSize,
            key: 'id'
        }
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Widget;
};